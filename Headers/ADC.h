/*
 * ADC.h
 *
 *  Created on: 24 sept. 2020
 *      Author: Thomas
 */

#ifndef HEADERS_ADC_H_
#define HEADERS_ADC_H_


void ADC_init();
void set_ADC14_interrupt();
void ADC_IRQHandler();



#endif /* HEADERS_ADC_H_ */
