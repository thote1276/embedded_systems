/*
 * timer.h
 *
 *  Created on: 24 sept. 2020
 *      Author: Thomas
 */

#ifndef HEADERS_TIMER_H_
#define HEADERS_TIMER_H_

void set_unit(int delay, int unit);
void set_timer(void);

void set_timerA1_interrupt(int delay, int unit);
void TA1_0_IRQHandler(void);




#endif /* HEADERS_TIMER_H_ */
