/*
 * PWM.h
 *
 *  Created on: 28 sept. 2020
 *      Author: Thomas
 */

#ifndef HEADERS_PWM_H_
#define HEADERS_PWM_H_

void PWM_init();
void PWM_servo(int ADC_value_x, int ADC_value_y);



#endif /* HEADERS_PWM_H_ */
