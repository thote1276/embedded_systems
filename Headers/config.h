/*
 * config.h
 *
 *  Created on: 24 sept. 2020
 *      Author: Thomas
 */

#ifndef HEADERS_CONFIG_H_
#define HEADERS_CONFIG_H_

#include "msp.h"

//Joystick
#define GPIO_PORT_JOYSTICK_X        P4
#define GPIO_DIR_JOYSTICK_X         ~BIT0  //P4.0 as input
#define ADC_CHANNEL_JOYSTICK_X      0
#define ADC_PORT_JOYSTICK_X         ADC14_MCTLN_INCH_13
#define SEL0_JOYSTICK_X             BIT0
#define SEL1_JOYSTICK_X             BIT0

#define GPIO_PORT_JOYSTICK_Y        P4
#define GPIO_DIR_JOYSTICK_Y         ~BIT1 //P4.1 as input
#define ADC_CHANNEL_JOYSTICK_Y      1
#define ADC_PORT_JOYSTICK_Y         ADC14_MCTLN_INCH_12
#define SEL0_JOYSTICK_Y             BIT1
#define SEL1_JOYSTICK_Y             BIT1

//Servos
#define GPIO_PORT_SERVO_X           P2
#define GPIO_DIR_SERVO_X            BIT4   //T0.1 as PWM in output
#define SEL0_SERVO_X                BIT4   //T0.1
#define SEL1_SERVO_X                BIT4  //T0.1

#define GPIO_PORT_SERVO_Y           P2
#define GPIO_DIR_SERVO_Y            BIT5   //T0.2 as PWM in output
#define SEL0_SERVO_Y                BIT5   //T0.2
#define SEL1_SERVO_Y                BIT5  //T0.2

#endif /* HEADERS_CONFIG_H_ */
