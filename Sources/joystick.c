/*
 * joystick.c
 *
 *  Created on: 24 sept. 2020
 *      Author: Thomas
 */

#include "config.h"
#include "msp.h"

void joystick_init(){
    GPIO_PORT_JOYSTICK_X->DIR |= GPIO_DIR_JOYSTICK_X;
    GPIO_PORT_JOYSTICK_X->SEL0 |= SEL0_JOYSTICK_X ;
    GPIO_PORT_JOYSTICK_X->SEL1 |= SEL1_JOYSTICK_X ;

    GPIO_PORT_JOYSTICK_Y->DIR |= GPIO_DIR_JOYSTICK_Y;
    GPIO_PORT_JOYSTICK_Y->SEL0 |= SEL0_JOYSTICK_Y ;
    GPIO_PORT_JOYSTICK_Y->SEL1 |= SEL1_JOYSTICK_Y ;
}


