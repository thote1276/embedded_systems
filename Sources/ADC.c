/*
 * ADC.c
 *
 *  Created on: 24 sept. 2020
 *      Author: Thomas
 */
#include "msp.h"
#include "config.h"
#include "PWM.h"

void ADC_init(){

    ADC14->CTL0 &=  ~ ADC14_CTL0_ENC;
    ADC14->CTL0 |= ADC14_CTL0_SHT0_7 | ADC14_CTL0_SHP | ADC14_CTL0_MSC | ADC14_CTL0_ON | ADC14_CTL0_CONSEQ_1; // on 16 bits,sampling par timer, adc on, ;
    ADC14->CTL1 &= ~ADC14_CTL1_RES0 & ~ADC14_CTL1_RES1;         // 10 bits conversion
    ADC14->CTL1 |= ADC14_CTL1_RES__10BIT;
    ADC14->MCTL[ADC_CHANNEL_JOYSTICK_X] |= ADC_PORT_JOYSTICK_X; //first channel will be for joystick X
    ADC14->MCTL[ADC_CHANNEL_JOYSTICK_Y] |= ADC_PORT_JOYSTICK_Y | //second channel will be for joystick Y
                                           ADC14_MCTLN_EOS;       //end of the sequence

    ADC14->IER0 |= ADC14_IER0_IE1; //put the interruption on channel 1

   // ADC14->CTL0 |= ADC14_CTL0_ENC | ADC14_CTL0_SC | ADC14_CTL0_CONSEQ_1; //Enable conversion

}


void set_ADC14_interrupt() {
    //We will use the timer 0.0 to use interruption
    NVIC_EnableIRQ(ADC14_IRQn);
    NVIC_SetPriority(ADC14_IRQn,1);

    ADC_init();
}


void ADC14_IRQHandler(){
    int l_joystick_value_x;
    int l_joystick_value_y;
    l_joystick_value_x = ADC14->MEM[ADC_CHANNEL_JOYSTICK_X]; //read clears the interruption
    l_joystick_value_y = ADC14->MEM[ADC_CHANNEL_JOYSTICK_Y]; //read clears the interruption
    PWM_servo(l_joystick_value_x, l_joystick_value_y);

}
