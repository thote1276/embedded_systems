/**
 * main.c
 */

#include "msp.h"
#include "ADC.h"
#include "timer.h"
#include "joystick.h"
#include "PWM.h"

//int l_joystick_value;

void main(void)
{
	WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer

	joystick_init();
	PWM_init();
	set_timerA1_interrupt(50, 0);
	set_ADC14_interrupt();

	while(1){

	}
}
