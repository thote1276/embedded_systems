/*
 * timer.c
 *
 *  Created on: 24 sept. 2020
 *      Author: Thomas
 */
#include "msp.h"

void set_timer(){
    TIMER_A1->CTL =MC__UP|TASSEL__SMCLK| TIMER_A_CTL_ID__8 ; //Up mode, Master clock (default 3 MHz), divided by 8
}
void set_unit(delay, unit){
    int l_tick_to_wait = (SystemCoreClock>>3)/1000*delay; //Calculate number of clk pulse to count
    TIMER_A1-> CCR[unit] = l_tick_to_wait; //registers where the set the amount to ocunt
    TIMER_A1-> CCTL[unit] = CCIE; //enabled the interruption
}

void set_timerA1_interrupt(delay, unit) {
    //We will use the timer 0.0 to use interruption
    NVIC_EnableIRQ(TA1_0_IRQn);
    NVIC_SetPriority(TA1_0_IRQn,2);

    set_timer();
    set_unit(delay, unit);
}

void TA1_0_IRQHandler(void) {
    TIMER_A1->CCTL[0] &= ~TIMER_A_CCTLN_CCIFG;     // Clear the flag
    ADC14->CTL0 |= ADC14_CTL0_ENC | ADC14_CTL0_SC ; //Enable conversion
}


