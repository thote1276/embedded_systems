/*
 * PWM.c
 *
 *  Created on: 27 sept. 2020
 *      Author: Thomas
 */

#include "msp.h"
#include "config.h"

void PWM_init(){
    GPIO_PORT_SERVO_X->DIR |= GPIO_DIR_SERVO_X;
    GPIO_PORT_SERVO_X -> SEL0 |= SEL0_SERVO_X;  //PWM on T0.1
    GPIO_PORT_SERVO_X -> SEL1 &= ~SEL1_SERVO_X;

    GPIO_PORT_SERVO_Y->DIR |= GPIO_DIR_SERVO_Y;
    GPIO_PORT_SERVO_Y -> SEL0 |= SEL0_SERVO_Y; //PWM on T0.2
    GPIO_PORT_SERVO_Y -> SEL1 &= ~SEL1_SERVO_Y;
}

void PWM_servo(ADC_value_x, ADC_value_y){

    TIMER_A0->EX0 = TAIDEX_2; // 1 MHz
    int l_tick_period = (SystemCoreClock/3)/1000*20; //Calculate number of clk pulse to count delay in ms
    TIMER_A0-> CCR[0] = l_tick_period; //registers where the set the amount to ocunt

    TIMER_A0->CCTL[1] = TIMER_A_CCTLN_OUTMOD_7;                                          // CCR1 reset/set
    TIMER_A0->CCR[1] = 1000+ ADC_value_x; //ADC value on 10 bits this way, its exactly the value to add to the offset

    TIMER_A0->CCTL[2] = TIMER_A_CCTLN_OUTMOD_7;                                          // CCR1 reset/set
    TIMER_A0->CCR[2] = 1000+ ADC_value_y; //ADC value on 10 bits this way, its exactly the value to add to the offset

    TIMER_A0->CTL = TASSEL__SMCLK | TIMER_A_CTL_MC_1  | TIMER_A_CTL_CLR;
}

