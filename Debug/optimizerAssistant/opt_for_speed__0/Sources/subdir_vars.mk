################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/ADC.c \
../Sources/GPIO.c \
../Sources/joystick.c \
../Sources/main.c \
../Sources/timer.c 

C_DEPS += \
./Sources/ADC.d \
./Sources/GPIO.d \
./Sources/joystick.d \
./Sources/main.d \
./Sources/timer.d 

OBJS += \
./Sources/ADC.obj \
./Sources/GPIO.obj \
./Sources/joystick.obj \
./Sources/main.obj \
./Sources/timer.obj 

OBJS__QUOTED += \
"Sources\ADC.obj" \
"Sources\GPIO.obj" \
"Sources\joystick.obj" \
"Sources\main.obj" \
"Sources\timer.obj" 

C_DEPS__QUOTED += \
"Sources\ADC.d" \
"Sources\GPIO.d" \
"Sources\joystick.d" \
"Sources\main.d" \
"Sources\timer.d" 

C_SRCS__QUOTED += \
"../Sources/ADC.c" \
"../Sources/GPIO.c" \
"../Sources/joystick.c" \
"../Sources/main.c" \
"../Sources/timer.c" 


